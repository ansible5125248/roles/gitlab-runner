# Gitlab-runner
Role for installing gitlab-runners from repository. Setup runners, register them in your gitlab instance. Create unique name for every runner. For now, it's possible to configure only docker or shell runners type. Also, you have an opportunity to set s3 cache storage

## Requirements
```
ansible > 2.7
```

```
pip install petname
```

## Role Tags
|Tag|Description|
|-|-|
|gitlab_runner_prerequisites|Set user, group, and install additional packages|
|gitlab_runner_install|Install runner from repo from current OS|
|gitlab_runner_unregister|Unregister and delete all runners from host|
|gitlab_runner_setup|Use **gitlab_runner_info** list and set some runners on your host|

## Role Variables
|Variable|Default|Description|Multivalue|Required|
|-|-|-|-|-|
|gitlab_runner_user|certbot_user|User for gitlab runner service|No|No|
|gitlab_runner_keyring|"/etc/apt/trusted.gpg.d/runner_gitlab-runner-archive-keyring.gpg"|Path to gpg keyring|No|No|
|gitlab_runner_config|"/etc/gitlab-runner/config.toml"|Set default config|No|No|
|gitlab_runner_info|see [defaults/main.yml](https://gitlab.calypsolabs.tech/calypso.finance/infrastructure/ansible/roles/gitlab-runner/-/blob/main/defaults/main.yml)|List of required variables|Yes|Yes|

## Additional descriptions
It set random name for runner using python **petname** module.
```
--description "{{ lookup('community.general.random_pet', words=2) }}-{{ reg_info.name }}"
```

## Dependencies
> [docker-engine](https://gitlab.calypsolabs.tech/calypso.finance/infrastructure/ansible/roles/docker-engine)

## TODO
- [ ] Make random name optional
